#!/usr/bin/env bash

if [ ! -f ./phpcs.phar ]; then
    echo "*** php code sniffer phar does not exists!!"
    exit 1
fi

#php phpcs.phar --standard=PSR12 --ignore=.gitlab -n --report=json --basepath=. . > ./php_code_sniffer_report.json

php phpcs.phar -n --report=json --basepath=. . > ./php_code_sniffer_report.json

php phpmd.phar . json cleancode,codesize,controversial,design,naming,unusedcode --report-file ./php_mass_detector.json --exclude vendor/*,.gitlab/* --ignore-errors-on-exit

if [ ! -f ./php_code_sniffer_report.json ]; then
    echo "*** php code sniffer report does not exists!!"
    exit 1
fi

php .gitlab/ci/code_quality_report_api.php --project_id=$1 --mr_id=$2 --token=$3 --endpoint=$4 --project_dir=$5
